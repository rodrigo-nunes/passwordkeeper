// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyAzbNyTjejjQFVbEDOd5DDySBNVDM7T63w',
    authDomain: 'pwd-keeper.firebaseapp.com',
    databaseURL: 'https://pwd-keeper.firebaseio.com',
    projectId: 'pwd-keeper',
    storageBucket: 'pwd-keeper.appspot.com',
    messagingSenderId: '517735056687'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
