export const environment = {
  production: true,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyAzbNyTjejjQFVbEDOd5DDySBNVDM7T63w',
    authDomain: 'pwd-keeper.firebaseapp.com',
    databaseURL: 'https://pwd-keeper.firebaseio.com',
    projectId: 'pwd-keeper',
    storageBucket: 'pwd-keeper.appspot.com',
    messagingSenderId: '517735056687'
  }
};
