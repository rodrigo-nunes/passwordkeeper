export class CustomMessages {

  private _success: boolean;
  private _message: any;

  constructor (success: boolean, message: any) {
    if (success) {
      this._setSuccess(message);
    } else {
      this._setError(message);
    }
  }

  private _setSuccess(message: any): any {
    this._success = true;
    this._message = message;
  }

  private _setError(message: any): any {
    this._success = false;

    switch (message.code) {
      case 'auth/invalid-email':
        this._message = 'E-mail inválido.';
        break;
      case 'auth/user-not-found':
        this._message = 'Não foi encontrada uma conta com este e-mail.';
        break;
      case 'auth/wrong-password':
        this._message = 'Senha incorreta.';
        break;
      case 'auth/network-request-failed':
        this._message = 'Ocorreu um erro na rede (como conexão interrompida, host inacessível ou tempo limite excedido).';
        break;
      case 'auth/email-already-in-use':
        this._message = 'O e-mail informado já está vinculado a uma conta.';
        break;
      case 'auth/weak-password':
        this._message = 'A senha parece não ser forte o suficiente. Considere usar letras (Maiúsculas/Minúsculas), números e símbolos';
        break;
      default:
        console.error(message);
        this._message = `
          Ocorreu um erro inesperado. Erro informado pelo serviço -> ${message.message}
          Para mais detalhes acesse o console do navegador.
        `;
        break;
    }

  }

  get success() {
    return this._success;
  }

  get message() {
    return this._message;
  }
}
