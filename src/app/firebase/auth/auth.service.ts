import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AppService } from '../../app.service';
import { Router } from '@angular/router';
import { CustomMessages } from './CustomMessages';
@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private authState: any = null;

  constructor (
    private afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.checkAuthentication();
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get currentUser(): firebase.auth.UserCredential {
    return this.authenticated ? this.authState : null;
  }

  private checkAuthentication() {
    const pathname = window.location.pathname;

    this.getObservableUser().subscribe(user => {
      if (pathname !== '/login') {
        if (!user) {
          this.router.navigate(['login']);
        }
      } else {
        if (user) {
          this.router.navigate(['dashboard']);
        }
      }
    });
  }

  emailSignUp(email: string, password: string, name: string): Promise<CustomMessages> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then( resp => {
        resp.user.updateProfile({
          displayName: name,
          photoURL: ''
        })
        .then( user => {
          this.authState = user;
        });

        return new CustomMessages(true, 'Conta registrada com sucesso. Aguarde...');
      })
      .catch( error => {
        return new CustomMessages(false, error);
      });
  }

  emailLogin(email: string, password: string): Promise<CustomMessages> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then( user => {
        this.authState = user;
      })
      .then( () => {
        return new CustomMessages(true, 'Login aceito');
      })
      .catch( error => {
        return new CustomMessages(false, error);
      });
  }

  resetPassword(email: string): Promise<CustomMessages> {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => {
        return new CustomMessages(true, 'Um e-mail foi encaminhado a você para que a senha seja redefinida');
      })
      .catch((error) => {
        console.log(error);
        return new CustomMessages(false, error);
      });
  }

  signOut(): Promise<void> {
    return this.afAuth.auth.signOut();
  }

  getObservableUser() {
    return this.afAuth.user;
  }
}
