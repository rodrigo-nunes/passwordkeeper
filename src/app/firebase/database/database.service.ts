import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private db: AngularFirestore) { }

  getDocumentions(collection: string) {
    return this.db.collection<any>(collection).valueChanges();
  }
}
