import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardElements } from './dashboard.elements';
import { AuthService } from 'src/app/firebase/auth/auth.service';
import { DatabaseService } from 'src/app/firebase/database/database.service';
import { Observable ,Subscription } from 'rxjs';
import { Password } from './model/Password';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  private _el: DashboardElements;
  private _passwordObservable: Observable<Password[]>;
  private _subscription: Subscription;
  passwords: Password[];

  constructor(private authService: AuthService, private db: DatabaseService) { }

  ngOnInit() {
    this._el = new DashboardElements();
    setTimeout(() => this.showDashboard(), 500);
    this.getPasswords();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  showDashboard() {
    this._el.self.classList.remove('is--hidden');
  }

  getPasswords() {
    this._passwordObservable = this.db.getDocumentions('passwords');
    this._subscription = this._passwordObservable.subscribe(data => {
      this.passwords = data;
    });
  }
}
