export class Password {

  private _name: string;
  private _password: string;
  private _description: string;

  constructor() { }

  get name() {
    return this._name;
  }

  get password() {
    return this._password;
  }

  get description() {
    return this._description;
  }

}