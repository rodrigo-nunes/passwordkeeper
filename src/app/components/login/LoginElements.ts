export class LoginElements {

  private _self: Element;
  private _wrap: Element;
  private _signIn: Element;
  private _signOut: Element;
  private _icon: Element;
  private _signUpIcon: Element;
  private _iconImg: Element;
  private _name: Element;
  private _password: Element;
  private _login: Element;
  private _forget: Element;

  constructor() {
    this._self = document.querySelector('.js--login');
    this._wrap = document.querySelector('.js--login-wrap');
    this._signIn = document.querySelector('.js--login-signIn');
    this._signOut = document.querySelector('.js--login-signUp');
    this._icon = document.querySelector('.js--login-icon');
    this._signUpIcon = document.querySelector('.js--login-signUp-icon');
    this._iconImg = document.querySelector('.js--login-icon-img');
    this._name = document.querySelector('.js--login-name');
    this._password = document.querySelector('.js--form-password');
    this._login = document.querySelector('.js--form-login');
    this._forget = document.querySelector('.js--form-forget');
  }

  get self() {
    return this._self;
  }

  get wrap() {
    return this._wrap;
  }

  get signIn() {
    return this._signIn;
  }

  get signOut() {
    return this._signOut;
  }

  get icon() {
    return this._icon;
  }

  get signUpIcon() {
    return this._signUpIcon;
  }

  get iconImg() {
    return this._iconImg;
  }

  get name() {
    return this._name;
  }

  get password() {
    return this._password;
  }

  get loginForm() {
    return this._login;
  }

  get forgetForm() {
    return this._forget;
  }
}
