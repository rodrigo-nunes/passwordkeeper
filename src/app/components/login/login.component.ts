import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { LoginElements } from './LoginElements';
import { AuthService } from 'src/app/firebase/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private el: LoginElements;
  loginForm: FormGroup;
  forgetForm: FormGroup;
  registerForm: FormGroup;

  constructor (
    private fb: FormBuilder,
    private router: Router,
    private appService: AppService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.el = new LoginElements();
    this.buildForms();

    setTimeout(() => {
      this.verifyHeightWindow();
      this.showLogin();
      this.showLogo();

      window.onresize = function(event) {
        this.verifyHeightWindow();
      }.bind(this);

    }, 500);
  }

  buildForms(): void {
    this.loginForm = this.fb.group({
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ])
      ]
    });

    this.forgetForm = this.fb.group({
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ]
    });

    this.registerForm = this.fb.group({
      name: [
        '',
        Validators.compose([
          Validators.required
        ])
      ],
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email
        ])
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ])
      ]
    });
  }

  login(): void {
    this.loginForm.disable();
    this.addClass(this.el.iconImg, 'is--loading');

    this.authService.emailLogin(this.loginEmail.value, this.loginPassword.value)
    .then( resp => {
      if (resp.success) {
        this.appService.alert({
          type: 'success',
          text: resp.message,
          delay: 2000
        });

        this.router.navigate(['dashboard']);
      } else {
        this.appService.alert({
          type: 'error',
          text: resp.message,
          delay: 10000
        });

        this.loginForm.enable();
        this.removeClass(this.el.iconImg, 'is--loading');
      }
    });
  }

  register() {
    this.registerForm.disable();
    this.addClass(this.el.signUpIcon, 'is--loading');

    this.authService.emailSignUp(this.registerEmail.value, this.registerPassword.value, this.registerName.value)
      .then( resp => {
        if (resp.success) {
          this.appService.alert({
            type: 'success',
            text: resp.message
          });
        } else {
          this.appService.alert({
            type: 'error',
            text: resp.message,
            delay: 10000
          });

          this.registerForm.enable();
          this.removeClass(this.el.signUpIcon, 'is--loading');
        }
      });
  }

  forgetPassword(): void {
    this.resetForms();
    this.toggleClass(this.el.loginForm, 'is--hidden');
    this.toggleClass(this.el.forgetForm, 'is--hidden');
  }

  resetPassword(): void {
    this.forgetForm.disable();
    this.addClass(this.el.iconImg, 'is--loading');

    this.authService.resetPassword(this.forgetEmail.value)
      .then( resp => {
        if (resp.success) {
          this.appService.alert({ type: 'success', text: resp.message});
          this.forgetForm.enable();
          this.removeClass(this.el.iconImg, 'is--loading');
        } else {
          this.appService.alert({ type: 'error', text: resp.message, delay: 10000 });
          this.forgetForm.enable();
          this.removeClass(this.el.iconImg, 'is--loading');
        }
      });
  }

  showLogin(): void {
    this.removeClass(this.el.self, 'is--hidden');
  }

  showLogo(): void {
    this.removeClass(this.el.icon, 'is--hidden');
    this.removeClass(this.el.name, 'is--hidden');
  }

  showSignUp(): void {
    this.resetForms();
    this.toggleClass(this.el.signIn, 'is--hidden');
    this.toggleClass(this.el.signOut, 'is--hidden');
  }

  toggleViewPassword($event): void {
    if ($event.checked) {
      this.el.password.setAttribute('type', 'text');
    } else {
      this.el.password.setAttribute('type', 'password');
    }
  }

  private addClass(e: Element, classe: string): void {
    e.classList.add(classe);
  }

  private removeClass(e: Element, classe: string): void {
    e.classList.remove(classe);
  }

  private toggleClass(e: Element, classe: string): void {
    e.classList.toggle(classe);
  }

  private verifyHeightWindow(): void {
    const windowHeight = window.innerHeight;
    const loginHeight  = this.el.signIn.clientHeight;
    const signUpHeight  = this.el.signOut.clientHeight;

    if (loginHeight >= windowHeight) {
      this.addClass(this.el.signIn, 'has--scroll');
    } else {
      this.removeClass(this.el.signIn, 'has--scroll');
    }

    if (signUpHeight >= windowHeight) {
      this.addClass(this.el.signOut, 'has--scroll');
    } else {
      this.removeClass(this.el.signOut, 'has--scroll');
    }
  }

  private resetForms(): void {
    this.registerForm.reset();
    this.loginForm.reset();
    this.forgetForm.reset();
  }

  get loginEmail(): any {
    return this.loginForm.get('email');
  }

  get loginPassword(): any {
    return this.loginForm.get('password');
  }

  get forgetEmail(): any {
    return this.forgetForm.get('email');
  }

  get registerName(): any {
    return this.registerForm.get('name');
  }

  get registerEmail(): any {
    return this.registerForm.get('email');
  }

  get registerPassword(): any {
    return this.registerForm.get('password');
  }

}
