import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavbarElements } from './NavbarElements';
import { AuthService } from 'src/app/firebase/auth/auth.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  private _el: NavbarElements;
  private _userObservable: Observable<firebase.User>;
  private _subscription: Subscription;
  userLogged: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this._el = new NavbarElements();
    this._userObservable = this.authService.getObservableUser();
    this._subscription = this._userObservable.subscribe(user => this.userLogged = user);
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
