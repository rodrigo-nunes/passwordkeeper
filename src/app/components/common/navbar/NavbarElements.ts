export class NavbarElements {

  private _self: Element;
  private _header: Element;
  private _items: Element;
  private _item: Element;
  private _link: Element;

  constructor() {
    this._self = document.querySelector('.js--navbar');
    this._header = document.querySelector('.js--navbar-header');
    this._items = document.querySelector('.js--navbar-items');
    this._item = document.querySelector('.js--navbar-item');
    this._link = document.querySelector('.js--navbar-link');
  }

  get self() {
    return this._self;
  }

  get header() {
    return this._header;
  }

  get items() {
    return this._items;
  }

  get item() {
    return this._item;
  }

  get link() {
    return this._link;
  }
}
