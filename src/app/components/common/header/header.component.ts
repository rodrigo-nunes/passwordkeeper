import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderElements } from './HeaderElements';
import { AuthService } from 'src/app/firebase/auth/auth.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private _el: HeaderElements;
  private _userObservable: Observable<firebase.User>;
  private _subscription: Subscription;
  userLogged: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this._userObservable = this.authService.getObservableUser();
    this._subscription = this._userObservable.subscribe(user => {
      this.userLogged = user;

      if (this.userLogged) {
        setTimeout(() => {
          this._el = new HeaderElements();
        }, 500);
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  toggleMenu() {
    this._el.menu.classList.toggle('is--toggle');
    this._el.navbar.classList.toggle('is--toggle');
    this._el.section.classList.toggle('menu-toggle');
  }
}
